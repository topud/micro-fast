package com.micro.fast.upms.dao;

import com.micro.fast.common.dao.SsmMapper;
import com.micro.fast.upms.pojo.UpmsRole;

public interface UpmsRoleMapper extends SsmMapper<UpmsRole,Integer> {
    int deleteByPrimaryKey(Integer id);

    int insert(UpmsRole record);

    int insertSelective(UpmsRole record);

    UpmsRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UpmsRole record);

    int updateByPrimaryKey(UpmsRole record);
}